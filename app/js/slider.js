(function() {

    let me = {};
    me.slick = function () {
    $('.sayings__case').slick({
        asNavFor: '.sayings__text',
        appendArrows: '.sayings__arrows',
        prevArrow: '<button type="button" class="sayings__button sayings__button-prev"><i class="page__prev fas' +
        ' fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="sayings__button sayings__button-next"><i class="page__next fas' +
        ' fa-chevron-right"></i></button>',
        dots: true,
        // dotsClass: 'dots'


    });

    $(".sayings__text").slick({
        asNavFor: '.sayings__case',
        appendArrows: '.sayings__arrows',
        prevArrow: '<button type="button" class="sayings__button sayings__button-prev"><i class="page__prev fas' +
        ' fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="sayings__button sayings__button-next"><i class="page__next fas' +
        ' fa-chevron-right"></i></button>'

    });

    };


RUS.slider = me;
RUS.slider.slick();
}());
